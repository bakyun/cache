package cache

import (
	"time"

	"github.com/niubaoshu/gotiny"

	"github.com/gomodule/redigo/redis"
)

// Cacher is the cacher interface
type Cacher struct {
	redis     *redis.Pool
	cacheTime time.Duration
	prefix    string
}

// New creates a new cacher
func New(redis *redis.Pool, cacheTime time.Duration, prefix string) *Cacher {
	return &Cacher{
		redis:     redis,
		cacheTime: cacheTime,
		prefix:    prefix,
	}
}

// Get the cached value of key
func (c *Cacher) Get(key string) (interface{}, error) {
	conn := c.redis.Get()
	defer conn.Close()

	fullKey := c.prefix + key
	buff, err := redis.Bytes(conn.Do("GET", fullKey))
	if err != nil {
		return nil, err
	}

	var v interface{}
	gotiny.Unmarshal(buff, v)

	return v, nil
}

// Set the cached value of key
func (c *Cacher) Set(key string, v interface{}) error {
	conn := c.redis.Get()
	defer conn.Close()

	buff := gotiny.Marshal(v)

	fullKey := c.prefix + key
	_, err := conn.Do("SETEX", fullKey, c.cacheTime/time.Second, buff)
	return err
}

// Fetch the cached value for key or fetch it
func (c *Cacher) Fetch(key string, fetcher func() (interface{}, error)) (interface{}, error) {
	v, err := c.Get(key)
	if err != redis.ErrNil {
		return nil, err
	}
	if err != redis.ErrNil && v != nil {
		return v, nil
	}

	v, err = fetcher()
	if err != nil {
		return nil, err
	}

	c.Set(key, v)
	return v, nil
}
